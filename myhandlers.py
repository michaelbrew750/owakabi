# myhandlers.py

from rapidsms.contrib.handlers import KeywordHandler
from rapidsms.contrib.handlers import PatternHandler

class HelpHandler(KeywordHandler):
    keyword = "help"

    def help(self):
        """Invoked if someone just sends `HELP`.  We also call this
        from `handle` if we don't recognize the arguments to HELP.
        """
        
        helpText = "Please send 'RIDE' if you would like to request a ride"
        self.respond(helpText);

    def handle(self, text):
        """Invoked if someone sends `HELP <any text>`"""
        text = text.strip().lower()
            
        """If we implement more detailed HELP commands, we can 
        do so here. For now, just send generic HELP text."""
        self.help()

class RideRequestHandler(KeywordHandler):
    keyword = "ride"
    
    def fetchLocations(self):
        locationList = []
        
        # Populate locationList with actual locations, hard code for now
        locationList.append("Zone 1")
        locationList.append("Zone 2")
        locationList.append("Zone 3")
        locationList.append("Zone 4")
        
        return locationList
    
    """This function is called if 'RIDE' is sent with no following text"""
    def help(self):
        responseText = "Please respond with the number corresponding to your location:\r"
        
        locationList = self.fetchLocations()
        counter = 1;
        
        for location in locationList:
            responseText += str(counter) + ": " + location + "\r"
            counter += 1
    
        self.respond(responseText);
    
    """This function is called if 'ride' is sent with some following text"""
    def handle(self, text):
        """Currently, we only have the 'ride' command, so ignore any text following it
            and just call our usual 'ride' response"""
        self.help()
